#!/usr/bin/env zsh
# Update ZLE buffer syntax highlighting.
#
# Invokes each highlighter that needs updating.
# This function is supposed to be called whenever the ZLE state changes.
_zsh_pygments()
{
    setopt localoptions nowarncreateglobal

    # Store the previous command return code to restore it whatever happens.
    local ret=$?

    {
        zpython 'zsh_pygments_highlight.highlight()'
        region_highlight=($ZSH_PYGMENTS_region_highlight)
    } always {
        return $ret
    }
    # Do not highlight if there are pending inputs (copy/paste).
    [[ $PENDING -gt 0 ]] && return $ret
    _zsh_pygments_buffer_modified || return $ret

}

# Whether the command line buffer has been modified or not.
#
# Returns 0 if the buffer has changed since _zsh_pygments was last called.
_zsh_pygments_buffer_modified()
{
    [[ "${_ZSH_HIGHLIGHT_PRIOR_BUFFER:-}" != "$BUFFER" ]]
}

typeset -g ZSH_PYGMENTS_MODULE_PATH=$0:A:h
# Setup python
_zsh_load_python()
{
    zmodload libzpython &>/dev/null || zmodload zsh/zpython &>/dev/null || return 1
    local MODPATH=$ZSH_PYGMENTS_MODULE_PATH
    zpython 'import sys,zsh; sys.path.append(zsh.getvalue("MODPATH"))' &&
    zpython 'import zsh_pygments_highlight; zsh_pygments_highlight.setup()'
}

# Rebind all ZLE widgets to make them invoke _zsh_pygmentss.
_zsh_pygments_bind_widgets()
{
    # Load ZSH module zsh/zleparameter, needed to override user defined widgets.
    zmodload zsh/zleparameter 2>/dev/null || {
        echo 'zsh-pygments-highlighting: failed loading zsh/zleparameter.' >&2
        return 1
    }

    # Override ZLE widgets to make them invoke _zsh_pygments.
    local cur_widget
    for cur_widget in ${${(f)"$(builtin zle -la)"}:#(.*|_*|orig-*|run-help|which-command|beep)}; do
        case $widgets[$cur_widget] in

            # Already rebound event: do nothing.
            user:$cur_widget|user:_zsh_pygments_widget_*);;

            # User defined widget: override and rebind old one with prefix "orig-".
            user:*) eval "zle -N orig-$cur_widget ${widgets[$cur_widget]#*:}; \
                                        _zsh_pygments_widget_$cur_widget() { builtin zle orig-$cur_widget -- \"\$@\" && _zsh_pygments }; \
                                        zle -N $cur_widget _zsh_pygments_widget_$cur_widget";;

            # Completion widget: override and rebind old one with prefix "orig-".
            completion:*) eval "zle -C orig-$cur_widget ${${widgets[$cur_widget]#*:}/:/ }; \
                                                    _zsh_pygments_widget_$cur_widget() { builtin zle orig-$cur_widget -- \"\$@\" && _zsh_pygments }; \
                                                    zle -N $cur_widget _zsh_pygments_widget_$cur_widget";;

            # Builtin widget: override and make it call the builtin ".widget".
            builtin) eval "_zsh_pygments_widget_$cur_widget() { builtin zle .$cur_widget -- \"\$@\" && _zsh_pygments }; \
                                         zle -N $cur_widget _zsh_pygments_widget_$cur_widget";;

            # Default: unhandled case.
            *) echo "zsh-pygments-highlighting: unhandled ZLE widget '$cur_widget'" >&2 ;;
        esac
    done
}

# Try loading python module
_zsh_load_python || {
    echo "zsh-pygments-highlighting: failed to setup python, exiting." >&2
    return 1
}

# Try binding widgets.
_zsh_pygments_bind_widgets || {
    echo 'zsh-pygments-highlighting: failed binding ZLE widgets, exiting.' >&2
    return 1
}

# Reset scratch variables when commandline is done.
_zsh_pygments_preexec_hook()
{
    zpython 'zsh_pygments_highlight.preexec()'
}
autoload -U add-zsh-hook
add-zsh-hook preexec _zsh_pygments_preexec_hook 2>/dev/null || {
        echo 'zsh-pygments-highlighting: failed loading add-zsh-hook.' >&2
    }

true
