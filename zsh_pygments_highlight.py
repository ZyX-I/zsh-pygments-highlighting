import zsh
import pygments
import pygments.lexers
import pygments.formatter
from pygments.console import codes
from pygments.util import get_choice_opt
from pygments.token import Keyword, Name, Comment, String, Error, \
     Number, Operator, Generic, Token, Whitespace

TERMINAL_COLORS = {
    Token:              ('',            ''),

    Whitespace:         ('lightgray',   'darkgray'),
    Comment:            ('lightgray',   'darkgray'),
    Comment.Preproc:    ('teal',        'turquoise'),
    Keyword:            ('darkblue',    'blue'),
    Keyword.Type:       ('teal',        'turquoise'),
    Operator.Word:      ('purple',      'fuchsia'),
    Name.Builtin:       ('teal',        'turquoise'),
    Name.Function:      ('darkgreen',   'green'),
    Name.Namespace:     ('_teal_',      '_turquoise_'),
    Name.Class:         ('_darkgreen_', '_green_'),
    Name.Exception:     ('teal',        'turquoise'),
    Name.Decorator:     ('darkgray',    'lightgray'),
    Name.Variable:      ('darkred',     'red'),
    Name.Constant:      ('darkred',     'red'),
    Name.Attribute:     ('teal',        'turquoise'),
    Name.Tag:           ('blue',        'blue'),
    String:             ('brown',       'brown'),
    Number:             ('darkblue',    'blue'),

    Generic.Deleted:    ('red',        'red'),
    Generic.Inserted:   ('darkgreen',  'green'),
    Generic.Heading:    ('**',         '**'),
    Generic.Subheading: ('*purple*',   '*fuchsia*'),
    Generic.Error:      ('red',        'red'),

    Error:              ('_red_',      '_red_'),
}

codes = {}

def setup_codes():
    global codes

    codes[""]          = None
    codes["reset"]     = None

    dark_colors  = ["black", "darkred", "darkgreen", "brown", "darkblue",
                    "purple", "teal", "lightgray"]
    light_colors = ["darkgray", "red", "green", "yellow", "blue",
                    "fuchsia", "turquoise", "white"]

    zsh_colors = ['black', 'red', 'green', 'yellow', 'blue', 'magenta', 'cyan', 'white']

    x = 0
    for d, l in zip(dark_colors, light_colors):
        codes[d] = ['fg='+zsh_colors[x]]
        codes[l] = ['bold']+codes[d]
        x += 1

    codes["darkteal"]   = codes["turquoise"]
    codes["darkyellow"] = codes["brown"]
    codes["fuscia"]     = codes["fuchsia"]
    codes["white"]      = ["bold"]

def zshformat(attr):
    result = []
    if attr[:1] == attr[-1:] == '+':
        # Good, blinking is not supported
        attr = attr[1:-1]
    if attr[:1] == attr[-1:] == '*':
        result.append('bold')
        attr = attr[1:-1]
    if attr[:1] == attr[-1:] == '_':
        result.append('underline')
        attr = attr[1:-1]
    if codes[attr]:
        result.extend(codes[attr])
    return result

class ZshFormatter(pygments.formatter.Formatter):
    def __init__(self, **kwargs):
        super(ZshFormatter, self).__init__(**kwargs)
        self.darkbg = get_choice_opt(kwargs, 'bg', ['light', 'dark'], 'light') == 'dark'
        self.offset = 0
        self.colorscheme = kwargs.get('colorscheme', None) or TERMINAL_COLORS

    def format_unencoded(self, tokensource, outfile):
        for ttype, value in tokensource:
            color = self.colorscheme.get(ttype)
            while color is None:
                ttype = ttype[:-1]
                color = self.colorscheme.get(ttype)
            if color:
                color = color[self.darkbg]
            if not isinstance(value, unicode):
                vlen = len(unicode(value, 'utf-8'))
            else:
                vlen = len(value)
            zfmt = zshformat(color)
            if zfmt:
                outfile.write(str(self.offset)+' '+str(self.offset+vlen)+' '+
                            (','.join(zfmt))+'\n')
            self.offset += vlen

def setup():
    global highlight
    global preexec
    lexer = pygments.lexers.get_lexer_by_name('sh')
    prevvars = {'BUFFER': None}
    setup_codes()
    def highlight():
        if zsh.getvalue('PENDING'):
            return
        BUFFER = zsh.getvalue('BUFFER')
        if prevvars['BUFFER'] == BUFFER:
            return
        tokens = pygments.lex(BUFFER, lexer)
        fmt = pygments.format(tokens, ZshFormatter())
        zsh.setvalue('ZSH_PYGMENTS_region_highlight', fmt.split('\n'))
        prevvars['BUFFER'] = BUFFER

    def preexec():
        prevvars['BUFFER'] = None
